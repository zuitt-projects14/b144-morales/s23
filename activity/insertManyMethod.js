db.hotels.insertMany(
[
{
    name: "double",
    accomodates: 3,
    price: 2000,
    description: "A room fit for a small family going on a vacation",
    rooms_available: 5,
    isAvailable: false
},
{
    name: "queen",
    accomodates: 4,
    price: 4000,
    descriptions: "A room with a queen sized bed perfect for a simple gateway",
    rooms_available: 15,
    isAvailable: false
}
]
)